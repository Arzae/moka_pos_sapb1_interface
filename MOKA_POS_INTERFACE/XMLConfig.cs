﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MOKA_POS_INTERFACE
{

    class XMLConfig
    {



        #region XML_CONFIG
        public string Server()
        {
            return GET("ServerName");
        }

        public string Database()
        {
            return GET("Database");
        }

        public string SQLUser()
        {
            return GET("SQLUser");
        }

        public string SQLPass()
        {
            return GET("SQLPass");
        }

        public string SAPUser()
        {
            return GET("SAPUser");
        }

        public string SAPPass()
        {
            return GET("SAPPass");
        }
        #endregion



        #region XML_FOLDERPATH

        public string Source_Folder()
        {
            return GET("Source_Folder");
        }


        public string Success_Folder()
        {
            return GET("Success_Folder");
        }
        public string Error_Folder()
        {
            return GET("Error_Folder");
        }

        public string Log_Folder()
        {
            return GET("Log_Folder");
        }

        #endregion



        //Read From XML File
        private string GET(string element)
        {
            Boolean take = false;
            string r = "";
            XmlTextReader reader = new XmlTextReader(@"config.xml");
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        if (reader.Name == element)
                        {
                            take = true;
                        }
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        if (take == true)
                        {
                            r = reader.Value;
                            take = false;
                        }
                        break;
                }

            }
            return r;
        }

    }
}

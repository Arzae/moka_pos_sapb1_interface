﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using Microsoft.VisualBasic.FileIO;
using System.ComponentModel;
using System.Globalization;

namespace MOKA_POS_INTERFACE
{
    class IF_ARInvoice
    {
        private enum SBO_column : int
        {
            IFNo = 0,
            TransNo = 1,
            LineNo = 2,
            CustomerCode = 3,
            PostingDate = 4,
            WarehouseCode = 5,
            ItemCode = 6,
            Qty = 7,
            Price = 8,
            Discount = 9,
        };

        public void EXEC_IF_ARInvoice(string pathIn, string pathOut_S, string pathOut_F, string pathLog, string fileCode)
        {
            try
            {
                if(pathIn.Contains("AR") == true)
                {
                    ReadCsv Csv = new ReadCsv();

                    DataTable DT;

                    DT = Csv.GetDataTabletFromCSVFile(pathIn);

                    int lRetCode;

                    if(DT.Rows.Count > 0)
                    {
                        //Import to SAPB1
                        SAPbobsCOM.Documents oInvoices;
                        bool SuccesFlag = false;
                        if(MainInterface.oCompany.InTransaction ==  false)
                        {
                            MainInterface.oCompany.StartTransaction();
                        }
                        int z = 0;
                        string sRefNo = "";
                        string sIFType = "";
                        string sFlag = "";

                        string TempRefNo = "";
                        string TempFlag = "";
                        string TempIFType = "";

                        int x = 0;

                    }
                }
            }
            catch(Exception Ex)
            {
                Console.WriteLine("Delivery Interface File :" + pathIn + " , Error : " + Ex.Message);

                fileLog(pathIn, Ex.Message, pathLog, false);
                fileMoving(pathIn, pathOut_F); //fail
            }
        }
    }
}

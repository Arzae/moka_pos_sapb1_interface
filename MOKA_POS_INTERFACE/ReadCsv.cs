﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using Microsoft.VisualBasic.FileIO;
using System.ComponentModel;


namespace MOKA_POS_INTERFACE
{
    class ReadCsv
    {
        public DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    //csvReader.ReadLine(); // Skip baris pertama
                    string[] colFields;

                    //create DataTable
                    bool tableCreated = false;
                    while (tableCreated == false)
                    {
                        colFields = csvReader.ReadFields();


                        foreach (string column in colFields)
                        {
                            DataColumn datacolumn = new DataColumn(column);
                            datacolumn.AllowDBNull = true;
                            csvData.Columns.Add(datacolumn);
                        }
                        tableCreated = true;

                    }


                    //input DataCsv to Datatable
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();

                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "" || fieldData[i] == null)
                            {
                                fieldData[i] = "";
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                    csvReader.Dispose();
                    csvReader.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            return csvData;
        }



        internal void GetDataTabletFromCSVFile()
        {
            throw new NotImplementedException();
        }


        public void WriteLog(string FileName, string ErrMsg, string pathLog, Boolean success)
        {


            // Simpan Interface Log Ke File
            //before your loop
            var csv = new StringBuilder();

            //in your loop

            string status = "Fail";
            if (success == true) { status = "Success"; }
            string date = (System.DateTime.Now).ToString();
            //Suggestion made by KyleMit
            var newLine = string.Format("{0};{1};{2};{3}", FileName, ErrMsg, status, date);
            csv.AppendLine(newLine);


            //after your loop
            File.AppendAllText(pathLog, csv.ToString());

        }

        public void WriteInitialLog(string code, string pathLog)
        {
            //before your loop
            var csv = new StringBuilder();

            //in your loop
            string date = (System.DateTime.Now).ToString();
            //Suggestion made by KyleMit
            var newLine = string.Format("{0};{1};{2}", code, "initial", date);
            csv.AppendLine(newLine);


            //after your loop
            File.AppendAllText(pathLog, csv.ToString());

        }

        public Boolean ExistDoc(string code, string pathLog)
        {
            Boolean r = false;
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(pathLog))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields;

                    //input DataCsv to Datatable
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();

                        //Making empty value as null
                        if (fieldData[0] == "" || fieldData[0] == null)
                        {
                            fieldData[0] = "";
                        }

                        if (code == fieldData[0]) { r = true; }

                    }
                    csvReader.Dispose();
                    csvReader.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return r;
        }
    }
}

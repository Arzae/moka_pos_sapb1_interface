﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using Microsoft.VisualBasic;
using System.Collections;
using System.Diagnostics;
using System.Threading;

//Program Name  : MainInterface.cs
//Description   : Main Interface Program Section
//Last Updated  : 06.05.2020
//Updated By    : Arzayoda

namespace MOKA_POS_INTERFACE
{
    class MainInterface
    {
        public static SAPbobsCOM.Company oCompany;
        public static string sErrMsg = null;
        public static int lErrCode = 0;
        public static int lRetCode;
        public static string sDBMode;
        public static string sInterfaceType;
        public static string sFileName;
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("HAUS MOKA POS - SAPB1 INTERFACE");
                Console.WriteLine("Version : ");
                Console.WriteLine("Developed by : PT. IDS Teknologi Indonesia");
                Console.WriteLine("");
                Console.WriteLine("");

                if (args.Length != 0)
                {
                    sInterfaceType = args[0];

                    //Read Configuration
                    XMLConfig Xml = new XMLConfig();

                    //Connecting to SAP DB
                    Console.WriteLine("You're running INSP Interface ... loading interface configuration...");

                    //Load Configuration
                    Console.WriteLine("");
                    Console.WriteLine("Interface Configuration :");
                    Console.WriteLine("1.Server   : " + Xml.Server().ToString() + "");
                    Console.WriteLine("2.DB Name  : " + Xml.Database().ToString() + "");
                    Console.WriteLine("3.DB User  : " + Xml.SQLUser().ToString() + "");
                    Console.WriteLine("4.SAP User : " + Xml.SAPUser().ToString() + "");
                    Console.WriteLine("");
                    Console.WriteLine("Connecting to SAP ... ");

                    oCompany = new SAPbobsCOM.Company();
                    oCompany.Server = Xml.Server().ToString();
                    //oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes)(6);
                    //oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);

                    //oCompany.UseTrusted = false;
                    oCompany.DbUserName = Xml.SQLUser().ToString();
                    oCompany.DbPassword = Xml.SQLPass().ToString();
                    oCompany.CompanyDB = Xml.Database().ToString();
                    oCompany.language = SAPbobsCOM.BoSuppLangs.ln_English;
                    oCompany.UserName = Xml.SAPUser().ToString();
                    oCompany.Password = Xml.SAPPass().ToString();

                    // Connecting to a company DB
                    lRetCode = oCompany.Connect();

                    if (lRetCode != 0)
                    {
                        int temp_int = lErrCode;
                        string temp_string = sErrMsg;
                        Console.Write("Not Connected [Error]");
                        Console.WriteLine("");
                        oCompany.GetLastError(out temp_int, out temp_string);
                        Console.WriteLine("Failed to Connect : " + oCompany.GetLastErrorDescription().ToString());

                        //Put Error Into Log Fileg

                    }
                    else
                    {
                        Console.Write("Connected [OK]");
                        Console.WriteLine("");

                        //Loading Interface
                        if (sInterfaceType != "")
                        {
                            Console.WriteLine("Processing Interface " + sInterfaceType);



                            if (sInterfaceType.Contains("MS") == true)
                            {
                                //Load Master Data Interface
                                Console.WriteLine("Master Data Interface ... Started");
                                ExecuteDocument(Xml.Source_Folder().ToString(), Xml.Success_Folder().ToString(), Xml.Error_Folder().ToString(), Xml.Log_Folder().ToString(), "", "MS");

                            }
                            if (sInterfaceType.Contains("TR") == true)
                            {
                                //Load Master Data Interface
                                Console.WriteLine("Transaction Data Interface ... Started");
                                ExecuteDocument(Xml.Source_Folder().ToString(), Xml.Success_Folder().ToString(), Xml.Error_Folder().ToString(), Xml.Log_Folder().ToString(), "", "TR");

                            }
                            if (sInterfaceType.Contains("DN") == true)
                            {
                                //Load Master Data Interface
                                Console.WriteLine("Delivery Data Interface ... Started");
                                ExecuteDocument(Xml.Source_Folder().ToString(), Xml.Success_Folder().ToString(), Xml.Error_Folder().ToString(), Xml.Log_Folder().ToString(), "", "DN");

                            }



                        }
                        else
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Interface Type Parameter is Missing !");

                            //Put Error Into Log File
                        }

                        Console.WriteLine("");
                        Console.WriteLine("SAP Interface Completed ... Please check result / error that might be happened");
                    }


                }
                else
                {
                    //Without Params
                    Console.WriteLine("You're running Interface Program without additional parameter");
                    Console.WriteLine("Cannot running SAP Interface. Contact your SAP Support");

                    //Put Error Into Log File
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("");
                Console.WriteLine("Error found : " + Ex.Message);
                Console.WriteLine("Cannot running SAP Interface. Contact your SAP Support");

                //  fileLog(pathIn, "Master Data Interface " + sIFType + " - " + sMode + " : " + sRefNo + ", Status : Success", pathLog, true);

                //Put Error Into Log File
            }
            //Console.ReadKey();
        }

        public static void ExecuteDocument(string pathIn, string pathOut_S, string pathOut_F, string pathLog, string fileCode, string document)
        {
            XMLConfig Config = new XMLConfig();
            ReadCsv Csv = new ReadCsv();

            var pathIn_temp = pathIn; //temp_pathIn

            string dirIn = pathIn;
            string dirOut_S = pathOut_S;
            string dirOut_F = pathOut_F;



            #region Scheduller
            string directoryPath = dirIn;
            DirectoryInfo dir = new DirectoryInfo(directoryPath);
            FileInfo[] files = dir.GetFiles("*.csv", SearchOption.AllDirectories);

            foreach (FileInfo file in files)
            {

                fileCode = file.Name;
                pathIn = dirIn + file.Name;
                pathOut_S = dirOut_S + file.Name;
                pathOut_F = dirOut_F + file.Name;


                switch (document)
                {

                    case "MS":
                        IF_MasterData oMD = new IF_MasterData();
                        oMD.EXEC_IF_MasterData(pathIn, pathOut_S, pathOut_F, pathLog, fileCode);
                        break;


                    case "TR":
                        IF_Transaction oTR = new IF_Transaction();
                        oTR.EXEC_IF_Transaction(pathIn, pathOut_S, pathOut_F, pathLog, fileCode);
                        break;


                    case "DN":
                        IF_Delivery oDN = new IF_Delivery();
                        oDN.EXEC_IF_Delivery(pathIn, pathOut_S, pathOut_F, pathLog, fileCode);
                        break;


                }

            }
            #endregion



        }
        private void fileLog(string FileName, string Msg, string pathLog, Boolean success)
        {
            ReadCsv csv = new ReadCsv();
            csv.WriteLog(FileName, "" + Msg + "", "" + pathLog + "", success);



        }
    }
}
